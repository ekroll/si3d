using System;
//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spaceship : MonoBehaviour {

    [SerializeField]
    private GameObject _pewPrefab;

    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Text _menuInfo;

    private float m_camera_fov_target = 25;
    private float m_camera_fov_min = 10;
    private float m_camera_fov_max = 25;

    private bool should_scope = false;

    private int score = 0;


    public void AddScorePoints(int points) {
        score += points;
        _scoreText.text = "Score: " + score.ToString();
    }


    private void OnTriggerEnter(Collider collider) {
        Debug.Log("Hit by " + collider.transform.name);

        if (collider.transform.tag == "Invader") {
            //Application.Quit();
        }
    }

    void Start() {
        Cursor.visible = false;
    }


    void Update() {
        Transform model = transform.GetChild(0);
        Transform camera = transform.GetChild(1);

        // From (0 <-> Screen.width) to (-0.5 <-> 0.5)  
        Vector3 mouse = new Vector3(         
            Input.mousePosition.x / Screen.width - 0.5f,
            Input.mousePosition.y / Screen.height - 0.5f,   
            0.0f
        ) * 2.0f; // Fixing range to +/- 1.0
        
        //Debug.Log(mouse);

        // Moving model
        model.position = Vector3.Lerp( 
            model.position,

            new Vector3(
                5.0f * -mouse.x,
                7.5f * -mouse.y, 
                0.0f
            ),

            Time.deltaTime * 10.0f
        );

        // Rotating model
        model.rotation = Quaternion.Euler(
           -30.0f * mouse.y, 
            30.0f * mouse.x,
           -30.0f * mouse.x
        );

        if (Input.GetMouseButtonDown(0)) {
            Instantiate(
                _pewPrefab,
                model.position + transform.forward * 10.0f,
                model.rotation
            );
        }

        // Rotating camera
        camera.rotation = Quaternion.Euler(
            camera.rotation.x,
            10.0f * mouse.x,
            camera.rotation.z
        );

        if (Input.GetMouseButtonDown(1)) {
            should_scope = true;
        }

        if (Input.GetMouseButtonUp(1)) {
            should_scope = false;
        }

        if (should_scope) {
            m_camera_fov_target = m_camera_fov_min;
        }

        else {
            m_camera_fov_target = m_camera_fov_max;
        }

        Camera cam_obj = Camera.main;

        if (cam_obj != null) {
            cam_obj.fieldOfView = Mathf.Lerp(
                cam_obj.fieldOfView, 
                m_camera_fov_target,
                Time.deltaTime * 10.0f
            );
        }

        if (Input.GetKeyDown("escape")) {
            _menuInfo.text = "Press ESC to pause (Haha, doesn`t work...)";
        }
    }
}
