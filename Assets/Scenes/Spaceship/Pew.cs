using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pew : MonoBehaviour
{

    private float dieTime = 2.0f;
    private float lifeTime = 0.0f;

    private bool haveKilledSomething = false;

    private Spaceship _spaceship;

    private void OnTriggerEnter(Collider collider) {
        if (haveKilledSomething) { return; }

        if (collider.transform.tag == "Invader") {
            haveKilledSomething = true;
            
            Destroy(collider.gameObject);

            _spaceship.AddScorePoints(
                collider.gameObject.GetComponent<Smallboy>().getKillPoints()
            );

            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start() {
        _spaceship = GameObject.Find("Spaceship").GetComponent<Spaceship>();
    }

    // Update is called once per frame
    void Update() {
        transform.Translate(Vector3.forward * Time.deltaTime * 240.0f);

        if (lifeTime >= dieTime) {
            Destroy(this.gameObject);
        }

        lifeTime += Time.deltaTime;
    }
}
