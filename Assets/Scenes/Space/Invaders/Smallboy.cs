using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smallboy : MonoBehaviour {

    Vector3 currentVector;
    
    float age = 0.0f;

    [SerializeField]
    float dieTime = 20.0f;

    private int killPoints;

    public void setKillPoints(int newPoints) {
        killPoints = newPoints;
    }

    public int getKillPoints() {
        return killPoints;
    }

    // Start is called before the first frame update
    void Start() {
        currentVector = new Vector3(
            Random.Range(-1, 1), 
            Random.Range(-1, 1), 
            Random.Range(-1, 1)
        );
    }

    // Update is called once per frame
    void Update() {
        Vector3 daWae = new Vector3(0.0f, 0.0f, 0.0f) - transform.position;
        daWae = Vector3.Normalize(daWae);
        
        currentVector = Vector3.Normalize(
            Vector3.Lerp(
                currentVector, 
                daWae, 
                Time.deltaTime
            )
        );
        
        transform.Translate(currentVector * Time.deltaTime * 15.0f);

        if (age >= dieTime) {
            Destroy(this.gameObject);
        }

        age += Time.deltaTime;
    }
}
