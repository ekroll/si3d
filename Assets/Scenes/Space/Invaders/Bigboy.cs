using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bigboy : MonoBehaviour {
    private float currentTime = 0.0f;
    private bool canSpawn = false;

    [SerializeField]
    private GameObject model;

    [SerializeField]
    private float startDelay = 0;

    [SerializeField]
    private float spawnInterval = 3;

    [SerializeField]
    private GameObject smallboyPrefab;

    [SerializeField]
    private int killPoints = 10;


    // Start is called before the first frame update
    void Start() {
        Instantiate(
            model,
            transform.position,
            transform.rotation,
            transform
        );
    }

    // Update is called once per frame
    void Update() {
        if (currentTime >= startDelay) {
            canSpawn = true;
        }

        if (canSpawn) {
            if (currentTime >= spawnInterval) {
                GameObject smallboy = Instantiate(
                    smallboyPrefab,
                    transform.position,
                    transform.rotation
                );

                smallboy.GetComponent<Smallboy>().setKillPoints(killPoints);

                Instantiate(
                    model,
                    smallboy.transform.position,
                    smallboy.transform.rotation,
                    smallboy.transform
                );

                currentTime = 0.0f;
            }

            //transform.position = new Vector3(
                //transform.position.x,
                //transform.position.y 
              //+ Mathf.Sin(
                    //Time.time 
                  //* Time.deltaTime
                  //* 60.0f
                //) 
              /// 10.0f,
               // transform.position.z
            //);
        }

        currentTime += Time.deltaTime;
    }
}
